const std = @import("std");
const w4 = @import("wasm4.zig");
const util = @import("util.zig");

const Self = @This();

pub const max_floor_bias = 18.0; // added to building exactly in middle
pub const min_floors = 3;
pub const max_floors = 9;
pub const min_columns = 4;
pub const max_columns = 8;

pub const column_pixels = 4;
pub const floor_pixels = 4;
pub const max_buildings = w4.SCREEN_SIZE / (min_columns * column_pixels) + 1;

pub const platform_num_columns: u8 = 5;
pub const platform_num_floors: u8 = 7;
pub const random_buildings_start_x: u8 = platform_num_columns * Self.column_pixels;
pub const random_buildings_stop_x: u8 = w4.SCREEN_SIZE - (platform_num_columns * Self.column_pixels);

pub const Design = enum { VLines, Windows, HLines };

// Struct fields
x_pos: u8,
num_floors: u8,
num_columns: u8,
design: Design,
color: u4,
secondary_color: u4,
tertiary_color: u4,

pub fn generateBuilding(rng: *std.rand.Random, x_pos: u8, max_x_pos: u8) Self {
    // Consider limited space for adjacent buildings
    const col_gap = (max_x_pos - x_pos) / column_pixels;
    const num_columns = if (col_gap < max_columns) blk: {
        break :blk col_gap;
    } else if (col_gap < max_columns + min_columns) blk: {
        break :blk min_columns;
    } else blk: {
        break :blk util.uintBetweenInclusive(rng, u8, min_columns, max_columns);
    };
    const middle_x = x_pos + (num_columns * column_pixels / 2);

    var colors = [_]u4{ 2, 3, 4 };
    rng.shuffle(u4, colors[0..]);
    return Self{
        .x_pos = x_pos,
        .num_floors = util.uintBetweenInclusive(rng, u8, min_floors, max_floors) + buildingBiasAtX(@floatFromInt(middle_x)),
        .num_columns = num_columns,
        .design = rng.enumValue(Design),
        .color = colors[0],
        .secondary_color = colors[1],
        .tertiary_color = colors[2],
    };
}

pub fn getHeight(self: Self) u8 {
    return self.num_floors * floor_pixels;
}

pub fn getWidth(self: Self) u8 {
    return self.num_columns * column_pixels;
}

pub fn getEndXPos(self: Self) u8 {
    return self.x_pos + self.getWidth();
}

pub fn getYPos(self: Self) u8 {
    return @as(u8, w4.SCREEN_SIZE) - self.getHeight();
}

pub fn draw(self: Self) void {
    w4.DRAW_COLORS.* = self.color;
    w4.rect(self.x_pos, self.getYPos(), self.getWidth(), self.getHeight());

    if (self.design == .Windows) {
        self.drawWindows();
    } else if (self.design == .VLines) {
        self.drawVLines();
    } else if (self.design == .HLines) {
        self.drawHLines();
    }
}

pub fn isPointWithin(self: Self, p_x: f32, p_y: f32) bool {
    // not checking pos higher than bottom of building
    const top: f32 = @floatFromInt(self.getYPos());
    const left: f32 = @floatFromInt(self.x_pos);
    const right: f32 = @floatFromInt(self.getEndXPos());
    const is_collision = p_y >= top and p_x >= left and p_x <= right;
    return is_collision;
}

fn drawHLines(self: Self) void {
    w4.DRAW_COLORS.* = self.secondary_color;
    for (0..self.num_floors) |row| {
        const y: u8 = self.getYPos() + (@as(u8, @intCast(row)) * floor_pixels);
        w4.hline(self.x_pos, y + column_pixels / 2, column_pixels * self.num_columns);
    }
}

fn drawVLines(self: Self) void {
    w4.DRAW_COLORS.* = self.secondary_color;
    for (0..self.num_columns) |col| {
        const x: u8 = self.x_pos + (@as(u8, @intCast(col)) * column_pixels);
        w4.vline(x + column_pixels / 2, self.getYPos(), self.num_floors * floor_pixels);
    }
}

fn drawWindows(self: Self) void {
    // w4.DRAW_COLORS.* = @as(u16, @intCast(self.secondary_color)) | (@as(u16, @intCast(self.tertiary_color)) << 4);
    w4.DRAW_COLORS.* = @as(u16, @intCast(self.secondary_color));
    for (0..self.num_columns) |col| {
        for (0..self.num_floors) |row| {
            // Find upper-left pixel x/y
            const x: u8 = self.x_pos + (@as(u8, @intCast(col)) * column_pixels);
            const y: u8 = self.getYPos() + (@as(u8, @intCast(row)) * floor_pixels);
            const x_margin: u8 = 1;
            const y_margin: u8 = 1;
            const win_x = x + x_margin;
            const win_y = y + y_margin;
            const win_width = column_pixels - (x_margin * 2);
            const win_height = floor_pixels - (y_margin * 2);
            w4.rect(win_x, win_y, win_width, win_height);
        }
    }
}

fn buildingBiasAtX(x: f32) u8 {
    // Parabolic distribution
    // Wasn't great because parabola drops off so quickly at ends,
    // it often leaves a large building in front of gorilla
    // const bias = std.math.pow(f32, x - 80.0, 2.0) / -400.0 + 16;

    // linear distribution
    const half_screen_width: f32 = @floatFromInt(w4.SCREEN_SIZE / 2);
    const distance_from_mid: f32 = @abs(half_screen_width - x);
    const bias = max_floor_bias - (distance_from_mid / half_screen_width) * max_floor_bias;
    return @intFromFloat(@round(bias));
}
