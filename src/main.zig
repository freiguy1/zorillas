const art = @import("art.zig");
const Building = @import("Building.zig");
const Gorilla = @import("Gorilla.zig");
const input_mode = @import("input_mode.zig");
const std = @import("std");
const thrown_mode = @import("thrown_mode.zig");
const util = @import("util.zig");
const w4 = @import("wasm4.zig");

pub const Mode = union(enum) {
    init,
    play,
    input: input_mode.InputRow,
    thrown: thrown_mode.ThrownState,
    destroyed_self,
    destroyed_other,
};

pub const physics = struct {
    pub const gravity: f32 = 0.03;
};

pub const building_hole_size = 8;
pub const BuildingHole = struct { x: u8, y: u8 };

pub const state = struct {
    pub var mode: Mode = .init;
    pub var rng_initialized = false;
    pub var rng: std.rand.Random = undefined;
    pub var buildings: std.BoundedArray(Building, Building.max_buildings) = undefined;
    pub var building_holes: util.RingBuffer(BuildingHole, 32) = .{};
    pub var gamepad: Gamepad = .{};
    pub var p_left_angle: u8 = 70; // 0 - 180
    pub var p_left_velocity: u8 = 50; // 0 - 100
    pub var p_right_angle: u8 = 110; // 0 - 180
    pub var p_right_velocity: u8 = 50; // 0 - 100
    pub var turn: enum { left, right } = .left;
    pub var num_ticks: u32 = 0; // total # of update calls since start overflows in a few years of running
    pub var left_gorilla: Gorilla = undefined;
    pub var right_gorilla: Gorilla = undefined;
};

pub const Gamepad = struct {
    prev: u8 = 0,
    pressed: u8 = 0,
    pressed_this_frame: u8 = 0,
    released_this_frame: u8 = 0,
    repeat_this_frame: u8 = 0, //if held, after a delay it'll count as a press
    held_count: [8]u32 = [_]u32{0} ** 8,

    // Pressed or repeated this frame
    pub fn clicked(self: Gamepad, btn_filter: u8) bool {
        return (self.pressed_this_frame | self.repeat_this_frame) & btn_filter != 0;
    }
};

export fn start() void {}

export fn update() void {
    // Setup button state
    const initial_wait_frames: u32 = 500 * 60 / 1000; // 500ms
    const subsequent_wait_frames: u32 = 50 * 60 / 1000; // 50ms
    const gamepad = w4.GAMEPAD1.*;
    state.gamepad.pressed = gamepad;
    state.gamepad.pressed_this_frame = gamepad & (state.gamepad.prev ^ gamepad);
    state.gamepad.released_this_frame = state.gamepad.prev & (state.gamepad.prev ^ gamepad);
    for (0..8) |i| {
        const filter: u8 = @as(u8, 1) << @as(u3, @intCast(i));
        if (gamepad & filter != 0) {
            state.gamepad.held_count[i] += 1;
        } else if (state.gamepad.released_this_frame & filter != 0) {
            state.gamepad.held_count[i] = 0;
        }

        if (state.gamepad.held_count[i] >= initial_wait_frames and (state.gamepad.held_count[i] - initial_wait_frames) % subsequent_wait_frames == 0) {
            // button has been held long enough, and we're on a frame which triggers a 'click'
            // set repeat_this_frame bit to 1
            state.gamepad.repeat_this_frame |= filter;
        } else {
            // set repeat_this_frame bit to 0
            state.gamepad.repeat_this_frame &= ~filter;
        }
    }

    // short loop if init mode
    if (state.mode == .init) {
        if (state.gamepad.clicked(w4.BUTTON_1 | w4.BUTTON_2)) {
            if (!state.rng_initialized) {
                var prng = std.rand.DefaultPrng.init(@intCast(state.num_ticks));
                state.rng = prng.random();
                state.rng_initialized = true;
                generateBuildings();
                initGorillas();
            }
            state.mode = Mode{ .input = .angle };
        }
        w4.DRAW_COLORS.* = 2;
        w4.text("Zorillas", 50, 60);
        w4.text("Press X/Z to start", 8, 90);
        w4.DRAW_COLORS.* = 4;
        state.gamepad.prev = gamepad;
        state.num_ticks += 1;
        return;
    }

    // Update state from physics and input
    switch (state.mode) {
        .input => |*input_row| {
            input_mode.updateState(input_row);
        },
        .thrown => |*thrown_state| {
            thrown_mode.updateState(thrown_state);
        },
        .destroyed_other => {
            if (state.gamepad.clicked(w4.BUTTON_1)) {
                generateBuildings();
                initGorillas();
                state.building_holes.clear();
                state.turn = switch (state.turn) {
                    .left => .right,
                    .right => .left,
                };
                state.mode = Mode{ .input = .angle };
            }
        },
        else => {},
    }

    // Draw things which appear in each (non-init) mode
    for (state.buildings.slice()) |b| {
        b.draw();
    }

    var hole_iter = state.building_holes.iter();
    while (hole_iter.next()) |hole| {
        w4.DRAW_COLORS.* = 0x11;
        w4.oval(
            hole.x - (building_hole_size / 2),
            hole.y - (building_hole_size / 2),
            building_hole_size,
            building_hole_size,
        );
    }

    state.left_gorilla.draw();
    state.right_gorilla.draw();

    // Draw mode-specific things
    switch (state.mode) {
        .input => |input_row| {
            input_mode.draw(input_row);
        },
        .thrown => |*thrown_state| {
            thrown_mode.draw(thrown_state);
        },
        .destroyed_other => {
            w4.DRAW_COLORS.* = 0x14;
            switch (state.turn) {
                .left => {
                    w4.text("Left player wins!", 12, 60);
                },
                .right => {
                    w4.text("Right player wins!", 9, 60);
                },
            }
            w4.text("Press X to restart", 8, 80);
        },
        else => {},
    }

    // Debugging to regenerate buildings
    // const gp = state.gamepad;
    // if (gp.pressed_this_frame & w4.BUTTON_1 != 0) {
    //     generateBuildings();
    //     initGorillas();
    // }

    state.gamepad.prev = gamepad;
    state.num_ticks += 1;
}

fn initGorillas() void {
    const left_building = state.buildings.get(0);
    const right_building = state.buildings.get(state.buildings.len - 1);
    state.left_gorilla = Gorilla{
        .x = left_building.x_pos + left_building.getWidth() / 2 - Gorilla.width / 2,
        .y = left_building.getYPos() - Gorilla.height,
    };

    state.right_gorilla = Gorilla{
        .x = right_building.x_pos + right_building.getWidth() / 2 - Gorilla.width / 2,
        .y = right_building.getYPos() - Gorilla.height,
    };
}

fn generateBuildings() void {
    state.buildings.resize(0) catch unreachable;

    // Generate left platform
    var left_platform = Building.generateBuilding(&state.rng, 0, Building.platform_num_columns * Building.column_pixels);
    left_platform.num_columns = Building.platform_num_columns;
    // left_platform.num_floors = Building.platform_num_floors;
    state.buildings.append(left_platform) catch unreachable;

    var prev_building_end: u8 = left_platform.getEndXPos();
    for (0..Building.max_buildings) |_| {
        var b = Building.generateBuilding(&state.rng, prev_building_end, Building.random_buildings_stop_x);

        state.buildings.append(b) catch unreachable;

        prev_building_end = b.getEndXPos();

        if (b.getEndXPos() == Building.random_buildings_stop_x) {
            break;
        }
    }

    var right_platform = Building.generateBuilding(&state.rng, prev_building_end, w4.SCREEN_SIZE);
    right_platform.num_columns = Building.platform_num_columns;
    // right_platform.num_floors = Building.platform_num_floors;
    state.buildings.append(right_platform) catch unreachable;
}

pub const AnimationState = struct {
    total_sprite_frames: u8,
    sprite_fps: u8,
    sprite_frame: u8 = 0,
    frame_counter: u8 = 0,

    const w4_fps = 60;

    fn getGameFramesPerSpriteFrame(self: AnimationState) u8 {
        return w4_fps / self.sprite_fps;
    }

    pub fn tick(self: *AnimationState) void {
        self.frame_counter = (self.frame_counter + 1) % self.getGameFramesPerSpriteFrame();
        if (self.frame_counter == 0) {
            self.sprite_frame = (self.sprite_frame + 1) % self.total_sprite_frames;
        }
    }
};
