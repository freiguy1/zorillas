const art = @import("art.zig");
const w4 = @import("wasm4.zig");
const std = @import("std");
const math = std.math;
const util = @import("util.zig");

const Self = @This();

pub const height: u8 = 12;
pub const width: u8 = 12;
const head_radius = 2;

x: u8, // left side
y: u8, // top side

fn getHeadPoint(self: Self) struct { x: u8, y: u8 } {
    return .{
        .x = self.x + width / 2,
        .y = self.y + 2,
    };
}

pub fn collidesWithPoint(self: Self, p_x: f32, p_y: f32) bool {
    // Check head collision
    const head = self.getHeadPoint();
    const h_x: f32 = @floatFromInt(head.x);
    const h_y: f32 = @floatFromInt(head.y);
    const distance_from_head_center = util.distBetweenPoints(h_x, h_y, p_x, p_y);
    const is_head_collision = distance_from_head_center <= head_radius + 1;

    // Check body collision
    const body_top: f32 = @floatFromInt(self.y + 4);
    const body_bottom: f32 = @floatFromInt(self.y + height - 1);
    const body_left: f32 = @floatFromInt(self.x + 2);
    const body_right: f32 = @floatFromInt(self.x + 10);
    const is_body_collision = p_y >= body_top and p_y <= body_bottom and p_x >= body_left and p_x <= body_right;

    return is_head_collision or is_body_collision;
}

pub fn draw(self: Self) void {
    art.drawGorilla(self.x, self.y, 0);
    // w4.DRAW_COLORS.* = 0x41;
    // const head = self.getHeadPoint();
    // w4.oval(head.x - head_radius, head.y - head_radius, head_radius * 2, head_radius * 2);

    // w4.rect(self.x + 2, self.y + 4, 8, 7);
}
