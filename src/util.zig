const std = @import("std");
const w4 = @import("wasm4.zig");

var buf: [300]u8 = undefined;

pub fn round(comptime T: type, i: anytype) T {
    return @as(T, @intFromFloat(@round(i)));
}

pub fn uintBetweenInclusive(rng: *std.rand.Random, comptime T: type, low: T, high: T) T {
    return rng.uintLessThanBiased(T, high - low + 1) + low;
}

pub fn log(comptime fmt: []const u8, args: anytype) void {
    const str = std.fmt.bufPrint(&buf, fmt, args) catch @panic("codebug");
    w4.trace(str);
}

pub fn printText(comptime fmt: []const u8, args: anytype, x: i32, y: i32) void {
    // i know, should handle error, but if doesn't work, game crashing seems okay
    const text = std.fmt.bufPrint(&buf, fmt, args) catch unreachable;
    w4.text(text, x, y);
}

pub fn fillRandomUintBelow(comptime T: type, rng: *std.rand.Random, s: []T, less_than: T) void {
    for (s) |*i| {
        i.* = rng.uintLessThan(T, less_than);
    }
}

pub fn distBetweenPoints(x1: f32, y1: f32, x2: f32, y2: f32) f32 {
    return std.math.sqrt(std.math.pow(f32, x1 - x2, 2) + std.math.pow(f32, y1 - y2, 2));
}

pub fn RingBuffer(comptime T: type, size: usize) type {
    const StaticLinearFifo = std.fifo.LinearFifo(T, .{ .Static = size });
    // std.debug.print("typeinfo: {?}\n", .{@typeInfo(StaticLinearFifo).Struct});
    return struct {
        fifo: StaticLinearFifo = StaticLinearFifo.init(),

        const Self = @This();

        pub fn add(self: *Self, item: T) void {
            if (self.fifo.writableLength() == 0) {
                self.fifo.discard(1);
            }
            self.fifo.writeItemAssumeCapacity(item);
        }

        pub fn realign(self: *Self) void {
            self.fifo.realign();
        }

        pub fn slice(self: *Self) []const T {
            return self.fifo.readableSliceOfLen(self.fifo.count);
        }

        pub fn clear(self: *Self) void {
            self.fifo.discard(self.fifo.count);
        }

        pub fn iter(self: Self) RingBufferIterator(T, size) {
            return .{ .fifo = self.fifo };
        }
    };
}

pub fn RingBufferIterator(comptime T: type, size: usize) type {
    const StaticLinearFifo = std.fifo.LinearFifo(T, .{ .Static = size });
    return struct {
        fifo: StaticLinearFifo,
        num_returned: usize = 0,
        const Self = @This();

        pub fn next(self: *Self) ?T {
            if (self.num_returned == self.fifo.count) {
                return null;
            } else {
                const buf_index = (self.fifo.head + self.num_returned) % self.fifo.buf.len;
                const item = self.fifo.buf[buf_index];
                self.num_returned += 1;
                return item;
            }
        }
    };
}

test {
    std.testing.refAllDecls(@This());
    _ = tests;
}

const tests = struct {
    var prng = std.rand.DefaultPrng.init(81);
    var random = prng.random();
    const expect = std.testing.expect;

    test "fill slice random uint below" {
        var array: [100]u8 = undefined;
        fillRandomUintBelow(u8, &random, array[0..], 100);
        for (array) |i| {
            try expect(i >= 0 and i < 100);
        }
    }

    test "can add items to ring buffer" {
        var rb = RingBuffer(usize, 5){};
        rb.add(23);
        try expect(rb.slice()[0] == 23);
    }

    test "can add more items than limit of ringbuffer" {
        var rb = RingBuffer(usize, 3){};
        rb.add(1);
        rb.add(2);
        rb.add(3);
        rb.add(4);

        const slice = rb.slice();
        try expect(slice.len == 3);
        try expect(slice[0] == 2);
        try expect(slice[1] == 3);
        try expect(slice[2] == 4);
    }

    const Point = struct { x: u8, y: u8 };

    // test "can add more struct items than limit of ringbuffer" {
    //     var rb = RingBuffer(Point, 3){};
    //     rb.add(.{ .x = 1, .y = 1 });
    //     rb.add(.{ .x = 2, .y = 2 });
    //     rb.add(.{ .x = 3, .y = 3 });
    //     rb.add(.{ .x = 4, .y = 4 });

    //     const slice = rb.slice();
    //     try expect(slice.len == 3);
    //     // try expect(slice[0] == 2);
    //     // try expect(slice[1] == 3);
    //     // try expect(slice[2] == 4);
    // }

    test "ringbuffer iterator" {
        var rb = RingBuffer(usize, 3){};
        rb.add(1);
        rb.add(2);
        rb.add(3);
        rb.add(4);
        var iter = rb.iter();
        try expect(iter.next() == 2);
        try expect(iter.next() == 3);
        try expect(iter.next() == 4);
        try expect(iter.next() == null);
    }

    test "clear ring buffer works" {
        var rb = RingBuffer(usize, 3){};
        rb.add(1);
        rb.add(2);
        rb.add(3);
        rb.add(4);
        try expect(rb.slice().len == 3);
        rb.clear();
        try expect(rb.slice().len == 0);
    }
};
