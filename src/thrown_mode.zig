const Banana = @import("Banana.zig");
const Gorilla = @import("Gorilla.zig");
const main = @import("main.zig");
const std = @import("std");
const util = @import("util.zig");
const w4 = @import("wasm4.zig");

const game_state = main.state;

pub const Collision = struct { x: f32, y: f32 };

pub const ThrownState = struct {
    start_tick: u32 = 0, // global state's num_tick when switched to this mode
    banana: Banana,

    // This method uses the values from input mode to set up thrown mode state
    pub fn init() @This() {
        const Values = struct {
            spin_clockwise: bool,
            position: struct { x: u8, y: u8 },
            angle: f32,
            velocity: f32,
        };

        const values: Values = switch (game_state.turn) {
            .left => .{
                .spin_clockwise = true,
                .position = .{
                    .x = game_state.left_gorilla.x + Gorilla.width / 2,
                    .y = game_state.left_gorilla.y,
                },
                // .position = .{ .x = 12, .y = 120 },
                .angle = @floatFromInt(game_state.p_left_angle),
                .velocity = @floatFromInt(game_state.p_left_velocity),
            },
            .right => .{
                .spin_clockwise = false,
                .position = .{
                    .x = game_state.right_gorilla.x + Gorilla.width / 2,
                    .y = game_state.right_gorilla.y,
                },
                .angle = @floatFromInt(game_state.p_right_angle),
                .velocity = @floatFromInt(game_state.p_right_velocity),
            },
        };

        // Raw values are 0-100
        const velocity_scale = 1.0 / 30.0; // scaled values are  0 - 2.5
        const velocity_min = 1.0; // shifted values are 1 - 3.5
        const radians: f32 = values.angle * std.math.pi / 180;
        const x_vel = std.math.cos(radians) * (values.velocity * velocity_scale + velocity_min);
        const y_vel = -std.math.sin(radians) * (values.velocity * velocity_scale + velocity_min);
        const banana: Banana = .{
            .spin_clockwise = values.spin_clockwise,
            .velocity = .{ .x = x_vel, .y = y_vel },
            .position = .{ .x = @floatFromInt(values.position.x), .y = @floatFromInt(values.position.y) },
        };
        return .{
            .start_tick = game_state.num_ticks,
            .banana = banana,
        };
    }
};

pub fn updateState(thrown_state: *ThrownState) void {
    var b = &thrown_state.banana;
    b.physics();

    // Check collisions with holes
    var hole_iter = game_state.building_holes.iter();
    const collides_with_hole = while (hole_iter.next()) |hole| {
        const dist = util.distBetweenPoints(b.position.x, b.position.y, @floatFromInt(hole.x), @floatFromInt(hole.y));
        if (dist <= @as(f32, @floatFromInt(main.building_hole_size)) / 2.0) {
            break true;
        }
    } else false;

    if (!collides_with_hole) {
        // Check collisions with buildings if not in a hole
        for (game_state.buildings.slice()) |building| {
            if (building.isPointWithin(b.position.x, b.position.y)) {
                game_state.building_holes.add(.{
                    .x = @intFromFloat(@round(b.position.x)),
                    .y = @intFromFloat(@round(b.position.y)),
                });

                // Update turn/mode
                game_state.turn = switch (game_state.turn) {
                    .left => .right,
                    .right => .left,
                };
                game_state.mode = main.Mode{ .input = .angle };
                return;
            }
        }
    }

    // Check collisions with gorillas
    if (game_state.turn == .right and game_state.left_gorilla.collidesWithPoint(b.position.x, b.position.y)) {
        game_state.mode = .destroyed_other;
    } else if (game_state.turn == .left and game_state.right_gorilla.collidesWithPoint(b.position.x, b.position.y)) {
        game_state.mode = .destroyed_other;
    }

    // If banana below screen, switch back to input mode
    if (thrown_state.banana.position.y > w4.SCREEN_SIZE) {
        game_state.turn = switch (game_state.turn) {
            .left => .right,
            .right => .left,
        };
        game_state.mode = main.Mode{ .input = .angle };
    }
}

pub fn draw(thrown_state: *ThrownState) void {
    thrown_state.banana.draw();
}
