const std = @import("std");
const Self = @This();
const util = @import("util.zig");
const w4 = @import("wasm4.zig");

pub usingnamespace (@import("generated_art.zig"));

const gorilla_color = 0x4032;
pub fn drawGorilla(x: i32, y: i32, flags: u32) void {
    w4.DRAW_COLORS.* = gorilla_color;
    w4.blit(
        &Self.gorilla,
        x,
        y,
        Self.gorilla_width,
        Self.gorilla_height,
        Self.gorilla_flags | flags,
    );
}

const banana_size = 5;
const banana = [_]u8{
    0b01100001,
    0b10001100,
    0b01100110,
    0b00000000,
    // 0b01100,
    // 0b00110,
    // 0b00110,
    // 0b00110,
    // 0b01100,
};

pub fn drawBanana(x: i32, y: i32, flags: u32) void {
    w4.DRAW_COLORS.* = 0x0030;
    w4.blit(&banana, x, y, banana_size, banana_size, w4.BLIT_1BPP | flags);
}

pub fn drawAngleLine(angle: u8, x: i32, y: i32) void {
    const length = 10.0; // pixels
    const radians: f32 = @as(f32, @floatFromInt(angle)) * std.math.pi / 180;
    const width = std.math.cos(radians) * length;
    const height = -std.math.sin(radians) * length; //negative because top-left is 0, 0
    w4.DRAW_COLORS.* = 4;
    w4.line(x, y, x + util.round(i32, width), y + util.round(i32, height));
    w4.DRAW_COLORS.* = 0x40;
    w4.oval(x - 2, y - 2, 5, 5);
}

pub fn drawArrow(x1: i32, y1: i32, x2: i32, y2: i32, tail_length: f32) void {
    const rotation = std.math.pi / 4.0;
    const cos_rotate = std.math.cos(rotation);
    const sin_rotate = std.math.sin(rotation);
    w4.line(x1, y1, x2, y2);
    const rise: f32 = @floatFromInt(y1 - y2);
    const run: f32 = @floatFromInt(x1 - x2);
    const magnitude: f32 = std.math.sqrt(std.math.pow(f32, run, 2.0) + std.math.pow(f32, rise, 2.0));
    const norm_x = run / magnitude;
    const norm_y = rise / magnitude;
    const tail1_x = norm_x * cos_rotate - norm_y * sin_rotate;
    const tail1_y = norm_x * sin_rotate + norm_y * cos_rotate;
    const tail2_x = norm_x * cos_rotate + norm_y * sin_rotate;
    const tail2_y = -norm_x * std.math.sin(rotation) + norm_y * cos_rotate;
    w4.line(x2, y2, x2 + util.round(i32, tail1_x * tail_length), y2 + util.round(i32, tail1_y * tail_length));
    w4.line(x2, y2, x2 + util.round(i32, tail2_x * tail_length), y2 + util.round(i32, tail2_y * tail_length));
}

pub fn drawPowerRect(power: u8, x: i32, y: i32, width: u32, height: u32) void {
    w4.DRAW_COLORS.* = 0x41;
    w4.rect(x, y, width, height);
    w4.DRAW_COLORS.* = 0x4;
    const val_width = @as(f32, @floatFromInt(power)) / 100.0 * (@as(f32, @floatFromInt(width - 4)));
    w4.rect(x + 2, y + 2, @intFromFloat(val_width), height - 4);
}
