const art = @import("art.zig");
const Banana = @import("Banana.zig");
const Building = @import("Building.zig");
const game_state = main.state;
const main = @import("main.zig");
const std = @import("std");
const thrown_mode = @import("thrown_mode.zig");
const util = @import("util.zig");
const w4 = @import("wasm4.zig");

pub const InputRow = enum {
    angle,
    velocity,

    fn next(self: *InputRow) void {
        const index: u32 = @intFromEnum(self.*);
        const nextNumber = (index + 1) % @typeInfo(@This()).Enum.fields.len;
        self.* = @enumFromInt(nextNumber);
    }

    fn prev(self: *InputRow) void {
        const cur_number = @intFromEnum(self.*);
        const len = @typeInfo(@This()).Enum.fields.len;
        const prev_number = if (cur_number == 0) len - 1 else cur_number - 1;
        self.* = @enumFromInt(prev_number);
    }
};

pub fn updateState(input_state: *InputRow) void {
    const gp = game_state.gamepad;
    const angle_var = switch (game_state.turn) {
        .left => &game_state.p_left_angle,
        .right => &game_state.p_right_angle,
    };
    if (input_state.* == .angle) {
        if (gp.clicked(w4.BUTTON_LEFT) and angle_var.* < 180) {
            angle_var.* += 1;
        }
        if (gp.clicked(w4.BUTTON_RIGHT) and angle_var.* > 0) {
            angle_var.* -= 1;
        }
    }

    const velocity_var = switch (game_state.turn) {
        .left => &game_state.p_left_velocity,
        .right => &game_state.p_right_velocity,
    };
    if (input_state.* == .velocity) {
        if (gp.clicked(w4.BUTTON_RIGHT) and velocity_var.* < 100) {
            velocity_var.* += 1;
        }
        if (gp.clicked(w4.BUTTON_LEFT) and velocity_var.* > 0) {
            velocity_var.* -= 1;
        }
    }
    if (gp.clicked(w4.BUTTON_DOWN)) {
        input_state.next();
    }
    if (gp.clicked(w4.BUTTON_UP)) {
        input_state.prev();
    }

    // Throw banana
    if (gp.clicked(w4.BUTTON_1)) {
        game_state.mode = main.Mode{ .thrown = thrown_mode.ThrownState.init() };
    }
}

pub fn draw(input_state: InputRow) void {
    // Draw inputs
    w4.DRAW_COLORS.* = 0x14;
    w4.text("Angle: ", 10, 17);
    w4.text("Power: ", 10, 31);
    w4.text("X to throw! ", 38, 45);
    switch (game_state.turn) {
        .left => {
            w4.text("Player 1", 10, 3);
            util.printText("{d} degrees", .{game_state.p_left_angle}, 80, 17);
            art.drawAngleLine(game_state.p_left_angle, 66, 22);
            art.drawPowerRect(game_state.p_left_velocity, 63, 31, 90, 8);
        },
        .right => {
            w4.text("Player 2", 10, 3);
            util.printText("{d} degrees", .{180 - game_state.p_right_angle}, 80, 17);
            art.drawAngleLine(game_state.p_right_angle, 66, 22);
            art.drawPowerRect(game_state.p_right_velocity, 63, 31, 90, 8);
        },
    }
    w4.DRAW_COLORS.* = 4;
    switch (input_state) {
        .angle => {
            art.drawArrow(1, 20, 7, 20, 3);
        },
        .velocity => {
            art.drawArrow(1, 34, 7, 34, 3);
        },
    }
}
