const std = @import("std");
const w4 = @import("wasm4.zig");
const util = @import("util.zig");
const art = @import("art.zig");
const main = @import("main.zig");
const AnimationState = main.AnimationState;
const physics_values = main.physics;

const Self = @This();

animation: AnimationState = AnimationState{ .sprite_fps = 7, .total_sprite_frames = 4 },
velocity: struct { x: f32, y: f32 } = .{ .x = 0, .y = -3 },
position: struct { x: f32, y: f32 } = .{ .x = 10, .y = 100 }, // middle of the banana
spin_clockwise: bool = true, // clockwise = left gorilla

pub fn physics(self: *Self) void {
    if (self.position.y < w4.SCREEN_SIZE + 50) {
        self.velocity.y += physics_values.gravity;
        self.position.x += self.velocity.x;
        self.position.y += self.velocity.y;
    }
}

pub fn draw(self: *Self) void {
    const flags = switch (self.animation.sprite_frame) {
        0 => 0,
        1 => if (self.spin_clockwise) w4.BLIT_ROTATE | w4.BLIT_FLIP_X else w4.BLIT_ROTATE,
        2 => w4.BLIT_FLIP_X,
        3 => if (self.spin_clockwise) w4.BLIT_ROTATE else w4.BLIT_ROTATE | w4.BLIT_FLIP_X,
        else => 0,
    };
    art.drawBanana(@intFromFloat(self.position.x - 2), @intFromFloat(self.position.y - 2), flags);

    self.animation.tick();
}
