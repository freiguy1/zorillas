# Zorillas

This game loosely follows the old school [Gorillas](https://en.wikipedia.org/wiki/Gorillas_(video_game)) game. The [WASM4](https://wasm4.org/) aspect ratio is quite a bit different, so the game definitely feels different.

Try playing the game [here](https://freiguy1.gitlab.io/zorillas/). It's deployed to Gitlab pages, check [.gitlab-ci.yml](.gitlab-ci.yml) for information on how it's built.

# Building & running

Build the cart by running: `zig build --release=small`

Then run it with: `w4 run zig-out/bin/cart.wasm`

Otherwise use `w4 watch` to continuously build & refresh browser upon file changes

# TODO

- [x] Render buildings
- [x] Randomize buildings
- [x] Add gorillas
- [x] Add banana, animation, physics
- [x] Add gorilla collisions
- [x] Add building collisions
- [x] Input UI (degrees and power)
- [x] Bias taller buildings toward middle
- [x] Gorilla buildings random instead of static

# Stretch goals

- [ ] Destructable buildings
- [ ] Gorilla throw animation
- [ ] Arrow which points at banana when flying out of frame
- [ ] Random building blinking lights
- [ ] Banana collision explosion animation
- [ ] Sun w/ collision animation
- [ ] Multi-player
